#!/usr/bin/python
''' Game of Bulls and Cows '''

# Random number generation
import random
# Regular expression hanfling
import re

# Length of the generated secret numebr
DIGITS = 4


def welcome():
    ''' Print welcoming message with instructions '''
    print "Hi there!"
    print "I've generated a random 4 digit number for you."
    print "Let's play a bulls and cows game."
    print "Enter a number"


def evaluate(tries):
    ''' Evaluate user's performance

    Args:
        tries: number of guesses user made

    '''
    print "Conratulations! That is the secret number!"
    if tries == 1:
        print "You are an oracle!"
    elif tries < 3:
        print "Awesome!"
    elif tries < 5:
        print "Good job!"
    elif tries < 8:
        print "Not so good, not so bad!"
    else:
        print "Try again and better..."


def generate_array():
    ''' Generate random 4-digit number

    Returns:
        array of 4 unique digit from range 0-9

    '''
    rand_array = []
    while len(rand_array) < DIGITS:
        num = random.randint(0, 9)
        # Digit must be unique in array context
        if num in rand_array:
            continue
        else:
            rand_array.append(num)
    return rand_array


def guess(secret):
    ''' Let user guess the secret number

    Args:
        secret: generated array of digits to be guessed
    Returns:
        wheter user guessed the secret or not

    '''
    num_string = raw_input(">>> ")
    # Check if input is correct
    if check_num(num_string) is None:
        print "This is not a 4-digit number..."
        return False
    # Initialize variables
    num_array = map(int, str(num_string))
    cows = 0
    bulls = 0
    # Check for bulls and cows
    for index, digit in enumerate(num_array):
        if secret[index] == digit:
            bulls += 1
        elif digit in secret:
            cows += 1
    # Print results of the guess
    guess_results(bulls, cows)
    # Did user guess the secret number?
    return bulls == DIGITS


def guess_results(bulls, cows):
    ''' Print results of a guess

    Args:
        bulls: number of bulls
        cows: number of cows

    '''
    print print_animal(bulls, "bull") + ', ' + print_animal(cows, "cow")


def check_num(num_string):
    ''' Check whether input is in correct format

    Args:
        num_string: user guess input
    Returns:
        whether input is in the correct format

    '''
    return re.match(r'^\d\d\d\d$', num_string)


def print_animal(number, animal):
    ''' Print number of bulls/cows with correct singular/plural ending

    Args:
        number: animal count
        animal: name of the animal
    Returns:
        correctly formated animal count string

    '''
    string = str(number) + ' ' + animal
    if number != 1:
        string += 's'
    return string


def game():
    ''' Game cycle '''
    # Print welcome message
    welcome()
    # Initialize variables
    tries = 0
    secret = generate_array()
    # Let the user guess and count his tries
    while guess(secret) != True:
        tries += 1
    # Evaluate results
    evaluate(tries)


# Start the game of startup
game()





